# AutomaticApiModelDecorator

Esta librería permite traducir los tipos usados en frontend a tipos válidos para la API con decoradores, sin necesidad de realizar funciones de mapeo manuales para cada entidad.

En el archivo [permiso.model.ts](/src/permiso.model.ts) se definen la clase Permiso de la siguiente forma:

```ts
class Permiso extends Entity {
  @Id()
  id_permiso: number;

  codigo: string;

  @BooleanType()
  activo: boolean;

  @RelatesTo(() => Permiso)
  permiso_padre: Permiso;

  @RelatesToMultiple(() => Usuario)
  usuarios: Usuario[];
}
```

De la misma manera, en el fichero [usuario.model.ts](/src/usuario.model.ts) se define la clase Usuario:
```ts
class Usuario extends Entity {
  @Id()
  id_usuario: number;

  nombre: string;

  @DateType()
  fecha_registro: Date;

  @BooleanType()
  activo: boolean;

  @RelatesToMultiple(() => Permiso)
  permisos: Permiso[];

  @JsonIgnore()
  propiedad_oculta: string;
}
```

Posteriormente, en el fichero [main.ts](/src/main.ts), se simula lo que podría ser un componente de Angular con una entidad Usuario rellena con datos provenientes de la API:

```ts
class EditarUsuarioComponent {
  entidad: Usuario;

  constructor(state: object) {
    this.entidad = new Usuario(state);
  }

  submit() {
    this.miServicio.alta(JSON.stringify(this.entidad))
  }
}
```

Finalmente se instancia esta clase con un mock de datos provenientes de la API y se muestra por pantalla el resultado de convertirlo desde el formato API a objetos de front y viceversa.

La conversión se realiza desde la clase `Entity` padre, por lo que el desarrollador solamente tiene que cargar los datos de la API en el constructor y se convierten a datos que se pueden tratar en front.

De la misma manera, cuando se llama a `JSON.stringify(this.entidad)`, JavaScript llama al método `toJSON`, y es quien realiza la conversión de vuelta al formato usado por la API

El resultado actual de ejecutar el programa es el siguiente:
```ts
Representacion en front: 
Usuario {
  id_usuario: 1297,
  nombre: 'Alejandro',
  fecha_registro: 2022-10-02T22:00:00.000Z,
  activo: true,
  propiedad_oculta: 'Propiedad oculta',
  permisos: [
    Permiso {
      id_permiso: 1,
      codigo: 'ADMIN',
      activo: true,
      permiso_padre: [Permiso],
      usuarios: [Array]
    },
    Permiso { 
      id_permiso: 26,
      codigo: 'NEW',
      activo: false
    }
  ]
}

Representacion en api: 
{
  "id_usuario": 1297,
  "nombre": "Alejandro",
  "fecha_registro": "2022-10-02T22:00:00.000Z",
  "activo": 1,
  "permisos": [
    {
      "id_permiso": 1
    },
    {
      "id_permiso": 26
    }
  ]
}

Representacion en front del permiso 0: 
Permiso {
  id_permiso: 1,
  codigo: 'ADMIN',
  activo: true,
  permiso_padre: Permiso { 
    id_permiso: 2, 
    codigo: 'ROOT' 
  },
  usuarios: [
    Usuario {
      id_usuario: 1297,
      nombre: 'Alejandro',
      fecha_registro: 2022-10-02T22:00:00.000Z,
      activo: true,
      permisos: [Array]
    }
  ]
}

Representacion en api del permiso 0:
{
  "id_permiso": 1,
  "codigo": "ADMIN",
  "activo": 1,
  "permiso_padre": {
    "id_permiso": 2
  },
  "usuarios": [
    {
      "id_usuario": 1297
    }
  ]
}
```