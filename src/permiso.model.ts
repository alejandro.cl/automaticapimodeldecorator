import { Entity } from "./lib/acl-entity";
import {
  Id,
  BooleanType,
  RelatesTo,
  RelatesToMultiple,
} from "./lib/acl-types.decorators";
import { Usuario } from "./usuario.model";

/**
 * La clase Permiso quedaría así:
 * class Permiso {
 *   id_permiso: number;
 *   key_id: string = "id_permiso";
 *
 *   codigo: string;
 *
 *   activo: boolean;
 *   _activo_domain_type: string = "BOOLEAN";
 *   _activo_api_type: string = "NUMBER";
 *
 *   permiso_padre: Permiso;
 *   _permiso_domain_type: string = "RELATION";
 *   _permiso_api_type: string = "RELATION";
 *   _permiso_constructor: Function = () => Permiso;
 *
 *    usuarios: Usuarios[];
 *   _usuarios_domain_type: string = "RELATION_MULTIPLE";
 *   _usuarios_api_type: string = "RELATION_MULTIPLE";
 *   _usuarios_constructor: Function = () => Usuario;
 * }
 */
export class Permiso extends Entity {
  @Id()
  id_permiso: number;

  codigo: string;

  @BooleanType()
  activo: boolean;

  @RelatesTo(() => Permiso)
  permiso_padre: Permiso;

  @RelatesToMultiple(() => Usuario)
  usuarios: Usuario[];
}
