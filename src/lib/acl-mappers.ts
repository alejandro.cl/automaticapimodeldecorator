import {
  BooleanToNumberMapping,
  DateToStringMapping,
  NumberToBooleanMapping,
  RelationApiToDomainMapping,
  RelationMultipleApiToDomainMapping,
  RelationMultipleToApiMapping,
  RelationToApiMapping,
  StringToDateMapping,
} from "./acl-mappers.functions";
import { ApiMappers, DomainMappers } from "./acl-mappers.types";

export const domainMappers: DomainMappers = {
  STRING: {
    DATE: StringToDateMapping,
  },
  NUMBER: {
    BOOLEAN: NumberToBooleanMapping,
  },
  RELATION: {
    RELATION: RelationApiToDomainMapping,
  },
  RELATION_MULTIPLE: {
    RELATION_MULTIPLE: RelationMultipleApiToDomainMapping,
  },
};

export const apiMappers: ApiMappers = {
  DATE: {
    STRING: DateToStringMapping,
  },
  BOOLEAN: {
    NUMBER: BooleanToNumberMapping,
  },
  RELATION: {
    RELATION: RelationToApiMapping,
  },
  RELATION_MULTIPLE: {
    RELATION_MULTIPLE: RelationMultipleToApiMapping,
  },
};
