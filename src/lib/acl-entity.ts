import { apiMappers, domainMappers } from "./acl-mappers";
import { ApiTypes, DomainTypes } from "./acl-types.enum";

export abstract class Entity {
  key_id: string; // Este campo se define desde el decorador @Id()

  constructor(apiModel?: object) {
    if (apiModel) {
      this.fromApi(apiModel);
    }
  }

  /**
   * Este metodo realiza la conversion de los tipos de la api a los tipos usados en el frontend
   * @param entity en formato de api
   */
  private fromApi(entity: object) {
    Object.keys(entity).forEach((key) => {
      const domainType: DomainTypes = this[`_${key}_domain_type`];
      const apiType: ApiTypes = this[`_${key}_api_type`];

      if (
        domainType === DomainTypes.RELATION ||
        domainType === DomainTypes.RELATION_MULTIPLE
      ) {
        const className = this[`_${key}_constructor`];
        this[key] = domainMappers[apiType][domainType](entity[key], className);
        return;
      }

      if (domainMappers[apiType] && domainMappers[apiType][domainType]) {
        this[key] = domainMappers[apiType][domainType](entity[key]);
      } else {
        this[key] = entity[key];
      }
    });
  }

  /**
   * Este metodo realiza la conversion de los tipos del frontend a los tipos usados en la api
   * Se le llama automaticamente cuando se hace un JSON.stringify del objeto
   * @returns entidad en formato de api
   */
  public toJSON() {
    return Object.keys(this).reduce((obj, key) => {
      const domainType: DomainTypes = this[`_${key}_domain_type`];
      const apiType: ApiTypes = this[`_${key}_api_type`];

      if (apiType === ApiTypes.IGNORE) return obj;

      if (apiMappers[domainType] && apiMappers[domainType][apiType]) {
        obj[key] = apiMappers[domainType][apiType](this[key]);
      } else {
        obj[key] = this[key];
      }

      return obj;
    }, {});
  }
}
