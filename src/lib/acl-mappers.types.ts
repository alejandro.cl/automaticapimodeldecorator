import { ApiTypes, DomainTypes } from "./acl-types.enum";

export type DomainMappers = {
  [T in keyof typeof ApiTypes]?: {
    [R in keyof typeof DomainTypes]?: (
      apiValue: any,
      ...extraParams: any[]
    ) => any;
  };
};

export type ApiMappers = {
  [T in keyof typeof DomainTypes]?: {
    [R in keyof typeof ApiTypes]?: (
      apiValue: any,
      ...extraParams: any[]
    ) => any;
  };
};
