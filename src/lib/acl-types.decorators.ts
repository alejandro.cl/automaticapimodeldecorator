import { Entity } from "./acl-entity";
import { ApiTypes, DomainTypes } from "./acl-types.enum";

/**
 * Este decorador define dos propiedades nuevas en el objeto para guardar cual
 * es el tipo usado en el front y en la API
 *
 * @param domainType tipo usado en front
 * @param apiType tipo usado para la API
 * @returns
 */
function DefineMapping(
  domainType: DomainTypes,
  apiType: ApiTypes,
  extraProperties?: { [_: string]: PropertyDescriptor }
) {
  return function (target: any, propertyKey: string) {
    Object.defineProperties(target, {
      [`_${propertyKey}_domain_type`]: { value: domainType },
      [`_${propertyKey}_api_type`]: { value: apiType },
      ...extraProperties,
    });
  };
}

export function BooleanType() {
  return DefineMapping(DomainTypes.BOOLEAN, ApiTypes.NUMBER);
}

export function DateType() {
  return DefineMapping(DomainTypes.DATE, ApiTypes.STRING);
}

/**
 * Este decorador evita que la propiedad forme parte del JSON
 * enviado a la API
 */
export function JsonIgnore() {
  return DefineMapping(DomainTypes.STRING, ApiTypes.IGNORE);
}

/**
 * Este decorador y el siguiente definen una propiedad extra con el
 * constructor de la clase relacionada, de esta forma podemos recrear
 * los objetos a partir de los datos de la API y viceversa
 *
 * @param relatedTo clase relacionada a esta propiedad de la entidad
 * @returns
 */
export function RelatesTo(
  relatedTo: () => { new (apiModel?: object): Entity }
) {
  return function (target: any, propertyKey: string) {
    DefineMapping(DomainTypes.RELATION, ApiTypes.RELATION, {
      [`_${propertyKey}_constructor`]: {
        get: () => relatedTo(),
      },
    })(target, propertyKey);
  };
}

export function RelatesToMultiple(
  relatedTo: () => {
    new (apiModel?: object): Entity;
  }
) {
  return function (target: any, propertyKey: string) {
    DefineMapping(DomainTypes.RELATION_MULTIPLE, ApiTypes.RELATION_MULTIPLE, {
      [`_${propertyKey}_constructor`]: {
        get: () => relatedTo(),
      },
    })(target, propertyKey);
  };
}

/**
 * Este decorador define una propiedad nueva en el objeto llamada key_id
 * que contiene el nombre de la propiedad que identifica al objeto
 *
 * En el caso de Permiso el resultado seria el siguiente:
 *
 * class Permiso {
 *   id_permiso: number;
 *
 *   key_id: string = "id_permiso";
 *
 *   ...
 * }
 */
export function Id() {
  return function (target: any, propertyKey: string) {
    Object.defineProperty(target, "key_id", {
      value: propertyKey,
    });
  };
}
