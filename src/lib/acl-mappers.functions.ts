import { Entity } from "./acl-entity";

export const StringToDateMapping = (value: string) => new Date(value);
export const DateToStringMapping = (value: Date) => value.toISOString(); // Define the date format used for the api

export const NumberToBooleanMapping = (value: number) => value === 1;
export const BooleanToNumberMapping = (value: boolean) => (value ? 1 : 0);

export const RelationToApiMapping = (value: Entity) => ({
  [value.key_id]: value[value.key_id],
});

export const RelationApiToDomainMapping = (
  value: object,
  className: { new (args: object): Entity }
) => new className(value);

export const RelationMultipleToApiMapping = (values: Entity[]) =>
  values.map(RelationToApiMapping);

export const RelationMultipleApiToDomainMapping = (
  values: object[],
  className: { new (args: object): Entity }
) => values.map((value) => RelationApiToDomainMapping(value, className));
