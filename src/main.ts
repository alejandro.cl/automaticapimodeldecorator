import { Usuario } from "./usuario.model";

class EditarUsuarioComponent {
  entidad: Usuario;

  constructor(state: object) {
    this.entidad = new Usuario(state);
  }

  submit() {
    console.log("Representacion en front:", this.entidad);
    console.log(
      "Representacion en api:",
      JSON.stringify(this.entidad, null, 2)
    );

    console.log(
      "Representacion en front del permiso 0:",
      this.entidad.permisos[0]
    );
    console.log(
      "Representacion en api del permiso 0: ",
      JSON.stringify(this.entidad.permisos[0], null, 2)
    );
  }
}

const datosApi = {
  id_usuario: 1297,
  nombre: "Alejandro",
  fecha_registro: "2022-10-03T00:00:00",
  activo: 1,
  propiedad_oculta: "Propiedad oculta",
  permisos: [
    {
      id_permiso: 1,
      codigo: "ADMIN",
      activo: 1,
      permiso_padre: { id_permiso: 2, codigo: "ROOT", activo: 1 },
      usuarios: [
        {
          id_usuario: 1297,
          nombre: "Alejandro",
          fecha_registro: "2022-10-03T00:00:00",
          activo: 1,
          permisos: [
            {
              id_permiso: 1,
              codigo: "ADMIN",
              activo: 1,
              permiso_padre: { id_permiso: 2, codigo: "ROOT", activo: 1 },
            },
          ],
        },
      ],
    },
    { id_permiso: 26, codigo: "NEW", activo: 0 },
  ],
};

new EditarUsuarioComponent(datosApi).submit();
