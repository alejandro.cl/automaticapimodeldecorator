import { Entity } from "./lib/acl-entity";
import {
  Id,
  DateType,
  BooleanType,
  RelatesToMultiple,
  JsonIgnore,
} from "./lib/acl-types.decorators";
import { Permiso } from "./permiso.model";

/**
 * La clase Usuario quedaría así:
 * class Usuario {
 *   id_usuario: number;
 *   key_id: string = "id_usuario";
 *
 *   nombre: string;
 *
 *   fecha_registro: Date;
 *   _fecha_registro_domain_type: string = "DATE";
 *   _fecha_registro_api_type: string = "STRING";
 *
 *   activo: boolean;
 *   _activo_domain_type: string = "BOOLEAN";
 *   _activo_api_type: string = "NUMBER";
 *
 *   permisos: Permiso[];
 *   _permisos_domain_type: string = "RELATION_MULTIPLE";
 *   _permisos_api_type: string = "RELATION_MULTIPLE";
 *   _permisos_constructor: Function = () => Permiso;
 *
 *
 *   propiedadOculta: string;
 *   _propiedad_oculta_domain_type = "STRING";
 *   _propiedad_oculta_api_type = "IGNORE";
 * }
 */
export class Usuario extends Entity {
  @Id()
  id_usuario: number;

  nombre: string;

  @DateType()
  fecha_registro: Date;

  @BooleanType()
  activo: boolean;

  @RelatesToMultiple(() => Permiso)
  permisos: Permiso[];

  @JsonIgnore()
  propiedad_oculta: string;
}
